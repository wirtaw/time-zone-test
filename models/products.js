let mongoose = require('mongoose');

let Schema = mongoose.Schema;

let productSchema = new Schema({
  title:  String,
  owner: String,
  description:   String,
  comments: [{ body: String, date: Date }],
  date: { type: Date, default: Date.now },
  hidden: Boolean,
  meta: {
    votes: Number,
    favs:  Number
  },
  qty: Number,
  qrcode: String,
  timeZone: String
});

module.exports = mongoose.model('Products', productSchema);
