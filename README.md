# Test time zone Javascript packages

## Project setup
```
npm install
```

### Create and save 5 product to database
```
npm run init
```

### Start API with products
```
npm run serve
```

### Start frontend part
```
npm run start
```

See it on http://localhost:8082
