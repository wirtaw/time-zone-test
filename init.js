'use strict';

const mongodb_conn_module = require('./mongodbConnModule');
mongodb_conn_module.connect();
const Products = require('./models/products');
const { DateTime } = require('luxon');

// const clientDateTime = "Wed May 1 2019 8:11:00 GMT+0300 (EET)";
// let dateTime = `${clientDateTime}`;
const eventTimeZoneName = "Europe/Kiev";
// let timeZone = `${eventTimeZoneName}`;

// console.dir(dateTime);
// console.dir(timeZone);
// console.dir(new Date(clientDateTime));
const now = DateTime.local();

let random = (min, max) => {
  return Math.floor(Math.random() * (max - min) + min);
};

let logArrayElements = (element, index) => {
  // console.log('list[' + index + '] = ' + element);
  let dt = DateTime.local(now.year, now.month, 1+index, now.hour + 1, now.minute);
  let newProduct = new Products({
    title: `Product${element}`,
    owner: 'Owner',
    description: `description${element} `,
    comments: [],
    date: dt.toString(),
    meta: {
      votes: random(0, 5),
      favs: random(0, 10)
    },
    qty: random(5, 100),
    qrcode: '',
    timeZone: eventTimeZoneName
  });
  // console.dir(dt.toString());
  // console.dir(newProduct);
  newProduct.save( (err) => {
    if (err) return console.error(err);
    // console.dir(newProduct.date);
  });
  //dt.plus({days: 1, hours: 2});
};
let list = [1, 2, 3, 4, 5];

setTimeout(() => {
  list.forEach(logArrayElements);
}, 5000);

setTimeout(() => {
  Products.find({ title: /^Product/ }, (err) => {
    // console.log(`Product list`)
    if (err) {
      console.error(err);
    }
    // console.dir(products._doc);
  });
}, 15000);
