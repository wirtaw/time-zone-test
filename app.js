'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const { DateTime } = require('luxon');

const app = express();
app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(cors());

const mongodb_conn_module = require('./mongodbConnModule');
const db = mongodb_conn_module.connect();

const Products = require('./models/products');

app.get('/products', (req, res) => {
  Products.find({}, 'id title owner date qty comments', (error, products) => {
    if (!error) {
      res.send({
        products: products.map((item) => {
          let product = {};
          for (let key in item._doc) {
            if (item._doc.hasOwnProperty(key)) {
              if (key !== 'publicId') {
                product[key] = item._doc[key]
              } else {
                product['id'] = item._doc[key]
              }
            }
          }
          return product
        })
      })
    } else {
      res.send({
        products: []
      })
    }
  }).sort({ _id: -1 })
});

app.get('/products_calendar', (req, res) => {
  const aggregate = [
    {
      $group : {
        _id : { month: { $month: "$date" }, day: { $dayOfMonth: "$date" }, year: { $year: "$date" } },
        averageQuantity: { $avg: "$qty" },
        count: { $sum: 1 }
      }
    },
    {
      $sort : {
        "_id.year": 1,
        "_id.month": 1,
        "_id.day": 1
      }
    }
  ];
  Products.
  aggregate(aggregate).
  exec((error, products) => {
    if (!error) {
      console.dir(products);
      res.send({
        products: products.map((item) => {
          let product = {};
          for (let key in item) {
            if (item.hasOwnProperty(key)) {
              product[key] = item[key]
              /*if (key !== '_id') {
                product[key] = item[key]
              } else {
                product['date'] = `${item[key].year}-${item[key].month}-${item[key].day}`
              }*/
            }
          }
          return product
        })
      })
    } else {
      res.send({
        products: []
      })
    }
  });
});

app.get('/products_today', (req, res) => {
  const now = DateTime.local();
  let yesterday = now.minus({hours: 12});
  let tommorow = now.plus({hours: 12});
  Products.find({ date: {
      $gt: yesterday.toString(),
      $lt: tommorow.toString()
    }}, 'id title owner date qty comments', (error, products) => {
    if (!error) {
      res.send({
        products: products.map((item) => {
          let product = {};
          for (let key in item._doc) {
            if (item._doc.hasOwnProperty(key)) {
              if (key !== 'publicId') {
                product[key] = item._doc[key]
              } else {
                product['id'] = item._doc[key]
              }
            }
          }
          return product
        })
      })
    } else {
      res.send({
        products: []
      })
    }
  }).sort({ _id: -1 })
});

app.post('/add_product', (req, res) => {
  let obj = { ...req.body };
  let product = {};
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      if (key !== 'id') {
        product[key] = obj[key]
      } else {
        product['publicId'] = obj[key]
      }
    }
  }
  const newProduct = new Products(product);
  
  newProduct.save(function (error) {
    res.send({
      success: !error
    })
  })
});


app.delete('/products/:id', (req, res) => {
  Products.remove({
    '_id': req.params.id
  }, (error) => {
    res.send({
      success: !error
    })
    
  })
});

app.listen(process.env.PORT || 8081);
